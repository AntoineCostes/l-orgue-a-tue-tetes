#include <ESP8266WiFi.h>

const String FIRMWARE_VERSION = "SL-v4.2";

//////////
// Leds //
//////////
#include <Adafruit_NeoPixel.h>
#define NUMPIXELS 180
#define NUMLAYERS 5
#define PIN_1      0
Adafruit_NeoPixel strip1(NUMPIXELS, PIN_1, NEO_GRB + NEO_KHZ800);
byte strip1_R[NUMLAYERS][NUMPIXELS];
byte strip1_G[NUMLAYERS][NUMPIXELS];
byte strip1_B[NUMLAYERS][NUMPIXELS];
#define PIN_2      5
Adafruit_NeoPixel strip2(NUMPIXELS, PIN_2, NEO_GRB + NEO_KHZ800);
byte strip2_R[NUMLAYERS][NUMPIXELS];
byte strip2_G[NUMLAYERS][NUMPIXELS];
byte strip2_B[NUMLAYERS][NUMPIXELS];

unsigned long lastLedTime = 0;
unsigned long ledRefreshMs = 20;

#include "SimplexNoise.h"
SimplexNoise sn;
int noise_dt = 100;
int noise_dx = 100;
int noise_offset = 100;
int noise_amplitude = 0;
float noise_t = 0.0;

///////////
// Serial /
///////////
#define INPUT_SIZE 50
char incBuffer[INPUT_SIZE];
unsigned long pingTimeInMs = 1000;
unsigned long lastPingTime = 0;
bool serialVerbose = false;

enum SERIAL_HEADER
{
  HANDSHAKE,
  VERBOSE,
  LED_STRIP_1,
  LED_STRIP_2,
  NOISE_AMPLITUDE,
  NOISE_DT,
  NOISE_DX,
  NOISE_OFFSET
};

void setup()
{
  // disable WIFI on ESP8266
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();

  // set up leds
  for (int l = 0; l < NUMLAYERS; l++)
    for (int i = 0; i < NUMPIXELS; i++)
    {
      strip1_R[l][i] = 0;
      strip1_G[l][i] = 0;
      strip1_B[l][i] = 0;

      strip2_R[l][i] = 0;
      strip2_G[l][i] = 0;
      strip2_B[l][i] = 0;
    }

  strip1.begin();
  strip1.fill(strip1.Color(0, 20, 0));
  strip1.show();
  strip2.begin();
  strip2.fill(strip2.Color(0, 20, 0));
  strip2.show();

  // set up serial
  Serial.begin(115200);
  delay(2000);
  handshake();

  //clear leds
  strip1.clear();
  strip1.show();
  strip2.clear();
  strip2.show();
}

void handshake()
{
  Serial.println("name:Guirlande");
  Serial.println("firmware:" + String(FIRMWARE_VERSION));
}

void loop()
{
  if (millis() - lastPingTime > pingTimeInMs)
  {
    Serial.println("id:5");
    lastPingTime = millis();
  }

  // process leds
  if (millis() - lastLedTime > ledRefreshMs)
  {
    float r, g, b;
    for (int i = 0; i < NUMPIXELS; i++)
    {
      // compute ambient noise
      noise_t += noise_dt * 0.000001;
      //int noise_int = 0;
      float noise_value1 = 1.0f;
      float noise_value2 = 1.0f;
      if (noise_amplitude > 0)
      {
        // noise between approx 0 and 1
        noise_value1 = sn.noise(noise_t, 0.1 * double(i * noise_dx) / 255.0) + 0.5f;
        // clip low values
        noise_value1 = constrain(noise_value1 - noise_offset / 255.0f, 0.0f, 1.0f);
        //modulate amplitude
        noise_value1 = constrain(noise_value1, 0.0f, 1.0f);

        // noise between approx 0 and 1
        noise_value2 = sn.noise(noise_t + 100, 0.1 * double(i * noise_dx) / 255.0) + 0.5f;
        // clip low values
        noise_value2 = constrain(noise_value2 - noise_offset / 255.0f, 0.0f, 1.0f);
        //modulate amplitude
        noise_value2 = constrain(noise_value1, 0.0f, 1.0f);
      }

      // ======== STRIP 1
      // blend the layers in screen mode
      r = g = b = 1.0f;
      for (int l = 0; l < NUMLAYERS; l++)
      {
        r *= (1.0f - strip1_R[l][i] / 255.0f);
        g *= (1.0f - strip1_G[l][i] / 255.0f);
        b *= (1.0f - strip1_B[l][i] / 255.0f);
      }
      r = (1.0f - r) * 255.0f;
      g = (1.0f - g) * 255.0f;
      b = (1.0f - b) * 255.0f;

      // add ambient noise
      if (i > 20) // hack pour masquer le tube
      {
        r = constrain(r + noise_amplitude * noise_value1, 0, 255);
        g = constrain(g + noise_amplitude * noise_value1, 0, 255);
        b = constrain(b + noise_amplitude * noise_value1, 0, 255);
      }
      // display
      strip1.setPixelColor(i, strip1.Color((int)r, (int)g, (int)b));

      // ======== STRIP 2
      // blend the layers in screen mode
      r = g = b = 1.0f;
      for (int l = 0; l < NUMLAYERS; l++)
      {
        r *= (1.0f - strip2_R[l][i] / 255.0f);
        g *= (1.0f - strip2_G[l][i] / 255.0f);
        b *= (1.0f - strip2_B[l][i] / 255.0f);
      }
      r = (1.0f - r) * 255.0f;
      g = (1.0f - g) * 255.0f;
      b = (1.0f - b) * 255.0f;

      // add ambient noise
      if (i > 20) // hack pour masquer le tube
      {
        r = constrain(r + noise_amplitude * noise_value2, 0, 255);
        g = constrain(g + noise_amplitude * noise_value2, 0, 255);
        b = constrain(b + noise_amplitude * noise_value2, 0, 255);
      }

      // display
      strip2.setPixelColor(i, strip2.Color((int)r, (int)g, (int)b));
    }
    strip1.show();
    strip2.show();
    lastLedTime = millis();
  }

  while (Serial.available()) {
    byte inputSize = Serial.readBytesUntil(255, incBuffer, INPUT_SIZE);
    if (serialVerbose) Serial.println(inputSize);

    switch (incBuffer[0])
    {
      case HANDSHAKE:
        if (inputSize != 1) break;
        handshake();
        break;

      case VERBOSE:
        if (inputSize != 1) break;
        serialVerbose = !serialVerbose;
        Serial.print("verbose:");
        if (serialVerbose)
          Serial.println("ON");
        else
          Serial.println("OFF");
        break;

      case LED_STRIP_1:
      case LED_STRIP_2:
        if (inputSize == 6)
        {
          int layerIndex = incBuffer[1];
          int ledIndex = incBuffer[2];
          int r = incBuffer[3];
          int g = incBuffer[4];
          int b = incBuffer[5];

          if (incBuffer[0] == LED_STRIP_1)
          {
            strip1_R[layerIndex][ledIndex] = r;
            strip1_G[layerIndex][ledIndex] = g;
            strip1_B[layerIndex][ledIndex] = b;
          } else
          {
            strip2_R[layerIndex][ledIndex] = r;
            strip2_G[layerIndex][ledIndex] = g;
            strip2_B[layerIndex][ledIndex] = b;
          }

          if (serialVerbose) Serial.println("set led");
        }
        else if (inputSize == 7)
        {
          int layerIndex = incBuffer[1];
          int ledIndex = incBuffer[2];
          int numLeds = incBuffer[3];
          int r = incBuffer[4];
          int g = incBuffer[5];
          int b = incBuffer[6];

          for (int i = ledIndex; i < ledIndex + numLeds && i < NUMPIXELS ; i++)
          {
            if (incBuffer[0] == LED_STRIP_1)
            {
              strip1_R[layerIndex][i] = r;
              strip1_G[layerIndex][i] = g;
              strip1_B[layerIndex][i] = b;
            } else
            {
              strip2_R[layerIndex][i] = r;
              strip2_G[layerIndex][i] = g;
              strip2_B[layerIndex][i] = b;
            }
          }
          if (serialVerbose) Serial.println("set led block");
        }
        break;

      case NOISE_AMPLITUDE:
        if (inputSize != 2) break;
        noise_amplitude = incBuffer[1];
        if (serialVerbose) Serial.println("set noise amplitude");
        break;

      case NOISE_DT:
        if (inputSize != 2) break;
        noise_dt = incBuffer[1];
        if (serialVerbose) Serial.println("set noise dt");
        break;

      case NOISE_DX:
        if (inputSize != 2) break;
        noise_dx = incBuffer[1];
        if (serialVerbose) Serial.println("set noise dx");
        break;

      case NOISE_OFFSET:
        if (inputSize != 2) break;
        noise_offset = incBuffer[1];
        if (serialVerbose) Serial.println("set noise offset");
        break;
    }
  }
}
