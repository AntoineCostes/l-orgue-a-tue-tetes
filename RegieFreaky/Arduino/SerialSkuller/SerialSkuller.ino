#include <ESP8266WiFi.h>

const String FIRMWARE_VERSION = "SS-v5.1";

#define MULTI_SERVO // COMMENTER POUR LA CHORALE

/////////////////
// ID and NAME //
/////////////////
#ifndef MULTI_SERVO
const int SKULL_ID = 5; // CHANGER LE NUMERO DU CRANE ICI : 1 à 4
#else
const int SKULL_ID = 0; // ne pas toucher
#endif
String SKULL_NAMES[6] = { "Jack", "Bob", "Jerry", "Mickael", "Sissi", "Led"};
const String SKULL_NAME = SKULL_NAMES[SKULL_ID];

///////////
// Servo //
///////////
#ifndef MULTI_SERVO
#include <Servo.h>
const int SERVO_PIN = 5;
Servo servo;
int servoMin = 50;
int servoMid = 75;
int servoMax = 100;

#else
#include <Adafruit_PWMServoDriver.h>
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
#define SERVOMIN  150 // this is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  574 // this is the 'maximum' pulse length count (out of 4096)
#endif
// aam, yes, no, jaw, eyesY, eyesX
int multiServoMin[6] =    {140, 25, 0, 54, 70, 99};
int multiServoMid[6] =  {160, 50, 90, 72, 85, 110};
int multiServoMax[6] =    {200, 75, 180, 84, 99, 120};

//////////
// Leds //
//////////
#include <Adafruit_NeoPixel.h>
#define PIN      0
#define NUMPIXELS 50
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


///////////
// Serial /
///////////
#define INPUT_SIZE 50
char incBuffer[INPUT_SIZE];
unsigned long pingTimeInMs = 1000;
unsigned long lastPingTime = 0;


void setup()
{
  // disable WIFI on ESP8266
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();

  // set up servo
#ifndef MULTI_SERVO
  pinMode(SERVO_PIN, OUTPUT);
  servo.attach(SERVO_PIN);
  servo.write(90); // init servo at mid-range
#else
  pwm.begin();
  pwm.setPWMFreq(60);  // Analog servos run at ~60 Hz updates

  // center motors
  for (int i = 0; i < 6; i++)
    pwm.setPWM(i, 0, map(multiServoMid[i], 0, 180, SERVOMIN, SERVOMAX));
#endif

  // set up leds
  pixels.begin();
  for (int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(0, 20, 0));
  }
  pixels.show();

  // set up serial
  Serial.begin(115200);
  delay(2000);
  handshake();
  pixels.clear();
  pixels.show();
}

void handshake()
{
  Serial.println("name:" + String(SKULL_NAME));
  Serial.println("firmware:" + String(FIRMWARE_VERSION));
}

void loop()
{
  if (millis() - lastPingTime > pingTimeInMs)
  {
    //Serial.println("ping");
    Serial.println("id:" + String(SKULL_ID));
    lastPingTime = millis();
  }

  while (Serial.available()) {
    byte inputSize = Serial.readBytesUntil(255, incBuffer, INPUT_SIZE);

    if (inputSize == 1 && incBuffer[0] == 0)
    {
      handshake();
      return;
    }

    if (inputSize == 3 && incBuffer[0] == 1)
    {
      int servoIndex = int(incBuffer[1]);
      int servoValue = int(incBuffer[2]);
      if (servoValue <= 180)
      {
#ifndef MULTI_SERVO
        if (servoValue < 90) servoValue = map(servoValue, 0, 90, servoMin, servoMid);
        else if (servoValue <= 180) servoValue = map(servoValue, 90, 180, servoMid, servoMax);
        servo.write(servoValue);
#else
        if (servoValue < 90) servoValue = map(servoValue, 0, 90, multiServoMin[servoIndex], multiServoMid[servoIndex]);
        else if (servoValue <= 180) servoValue = map(servoValue, 90, 180, multiServoMid[servoIndex], multiServoMax[servoIndex]);
        pwm.setPWM(servoIndex, 0, map(servoValue, 0, 180, SERVOMIN, SERVOMAX));
#endif
      Serial.println("servoValue:" + String(servoValue));
      }
    }

    if (inputSize == 5 && incBuffer[0] == 2)
    {
      int ledIndex = incBuffer[1];
      int r = incBuffer[2];
      Serial.println("r:" + String(r));
      int g = incBuffer[3];
      Serial.println("g:" + String(g));
      int b = incBuffer[4];
      Serial.println("b:" + String(b));

      pixels.setPixelColor(ledIndex, pixels.Color(r, g, b));
      pixels.show();
    }
    if (inputSize == 6 && incBuffer[0] == 2)
    {
        int ledIndex1 = incBuffer[1];
        int ledIndex2 = incBuffer[2];
        int r = incBuffer[3];
        Serial.println("r:" + String(r));
        int g = incBuffer[4];
        Serial.println("g:" + String(g));
        int b = incBuffer[5];
        Serial.println("b:" + String(b));

        for (int i = min(ledIndex1, ledIndex2); i <= max(ledIndex1, ledIndex2); i++)
        pixels.setPixelColor(i, pixels.Color(r, g, b));
      pixels.show();
    }
  }
}
