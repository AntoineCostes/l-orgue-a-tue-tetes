var HANDSHAKE = 0;
var SERVO = 1;

function dataReceived(data)
{
	script.log(data);

	var name = data.split(":")[0];
	var value = data.split(":")[1];
	if (name == "id")
	{
		local.parameters.oscPing.set("/"+local.values.id.get()+"/"+local.values.nom.get()+"/"+local.values.firmware.get()+"/ping");
		local.values.ping.trigger();
	}
	if (name == "name")
	{
		local.values.nom.set(value); // workaround to get a working script control address
	}
}

function handshake()
{
	local.sendBytes(HANDSHAKE, 255);
}

function setServo(servo, value)
{
	var min = 0;
	var max = 0;
	var invert = 0;

	if (servo == 0)
	{
		min = local.parameters.aam.min.get();
		max = local.parameters.aam.max.get();
		invert = local.parameters.aam.invert.get();
	}
	else if (servo == 1)
	{
		min = local.parameters.yes.min.get();
		max = local.parameters.yes.max.get();
		invert = local.parameters.yes.invert.get();
	}
	else if (servo == 2)
	{
		min = local.parameters.no.min.get();
		max = local.parameters.no.max.get();
		invert = local.parameters.no.invert.get();
	}
	else if (servo == 3)
	{
		min = local.parameters.jaw.min.get();
		max = local.parameters.jaw.max.get();
		invert = local.parameters.jaw.invert.get();
	}
	else if (servo == 4)
	{
		min = local.parameters.eyesy.min.get();
		max = local.parameters.eyesy.max.get();
		invert = local.parameters.eyesy.invert.get();
	}
	else if (servo == 5)
	{
		min = local.parameters.eyesx.min.get();
		max = local.parameters.eyesx.max.get();
		invert = local.parameters.eyesx.invert.get();
	}
	else return;

	value = (value + 1)*0.5; // cast to 0-1
	if (invert) value = 1 - value;
	var val = min + value*(max - min);
	local.sendBytes(SERVO, servo, val, 255);

	script.log(servo, val);
}

function setJaw(value)
{
	min = local.parameters.jaw.min.get();
	max = local.parameters.jaw.max.get();
	invert = local.parameters.jaw.invert.get();
	if (invert) value = 1 - value;
	var val = min + value*(max - min);
	local.sendBytes(SERVO, 3, val, 255);
}
